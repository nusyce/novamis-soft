{
    "name": "Project Task as Product",
    "summary": "Record product spent in a Task",
    "version": "12.0.1.0.0",
    "category": "Project Management",
    "author": "NUSYCE",
    "website": "https://github.com/OCA/project",
    "license": "AGPL-3",
    "installable": True,
    "depends": [
        "project",
        "product",
    ],
    "data": [
        "views/project_view.xml",
    ],
}
