from odoo import api, fields, models, exceptions, _
from odoo.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class ProjectProject(models.Model):
    _inherit = 'project.project'

    total_price = fields.Float(string='Total Price', compute='_pj_compute_amount')

    @api.one
    @api.depends('task_ids.total_price')
    def _pj_compute_amount(self):
        self.total_price = sum(line.total_price for line in self.task_ids)


class Task(models.Model):
    _inherit = "project.task"

    quantity = fields.Float(string='Quantity', default=1, store=True)
    old_quantity = fields.Float(string='Old Quantity', default=1, store=True)
    price = fields.Float(string='Unit Price', store=True)
    total_price = fields.Monetary(string='Total Price', store=True, compute='_compute_price',
                                  currency_field='company_currency', track_visibility='always')

    stock = fields.Float(string='Stock', store=True, compute='_compute_stock', track_visibility='always')

    product_id = fields.Many2one(
        comodel_name='product.product', string='Product',
        domain="[('type', 'in', ('consu', 'product'))]", required=True, store=True)
    company_currency = fields.Many2one(string='Currency', related='company_id.currency_id', readonly=True,
                                       relation="res.currency")

    stock_move_id = fields.Many2one(
        comodel_name='stock.move',
        string='Stock Move',
    )

    currency_id = fields.Many2one("res.currency", string="Currency")

    location_source_id = fields.Many2one(
        comodel_name='stock.location',
        string='Source Location',
        index=True,
        help='Keep this field empty to use the default value from'
             ' the project.',
    )

    location_dest_id = fields.Many2one(
        comodel_name='stock.location',
        string='Destination Location',
        index=True,
        help='Default location to which materials are consumed.',
    )

    product_uom_id = fields.Many2one(
        comodel_name='uom.uom',
        oldname="product_uom",
        string='Unit of Measure',
    )

    @api.one
    @api.depends('quantity', 'price')
    def _compute_price(self):
        self.total_price = self.price * self.quantity

    @api.multi
    def update_stock_move(self):
        product = self.product_id
        _logger.debug(product.id)
        res = {
            'product_id': product.id,
            'name': product.partner_ref,
            'state': 'done',
            'product_uom': self.product_uom_id.id or product.uom_id.id,
            'product_uom_qty': self.quantity,
            'origin': self.name,
            'location_id':
                self.location_source_id.id or
                self.env.ref('stock.stock_location_stock').id,
            'location_dest_id':
                self.location_dest_id.id or
                self.env.ref('stock.stock_location_customers').id,
        }
        move = self.env['stock.move'].create(res)
        move._action_confirm()
        move._action_assign()
        move.move_line_ids.write(
            {'qty_done': self.quantity})  # This creates a stock.move.line record. You could also do it manually
        move._action_done()

    @api.multi
    def unlink(self):
        self.unlink_stock_move()
        # if self.stock_move_id:
        #     raise exceptions.Warning(
        #         _("You can't delete a product if already "
        #           "have stock movements done.")
        #     )
        # self.analytic_line_id.unlink()
        return super(Task, self).unlink()

    @api.multi
    def unlink_stock_move(self):
        if not self.stock_move_id.state == 'done':
            if self.stock_move_id.state == 'assigned':
                self.stock_move_id._do_unreserve()
            if self.stock_move_id.state in (
                    'waiting', 'confirmed', 'assigned'):
                self.stock_move_id.write({'state': 'draft'})
            picking_id = self.stock_move_id.picking_id
            self.stock_move_id.unlink()
            if not picking_id.move_line_ids_without_package and \
                    picking_id.state == 'draft':
                picking_id.unlink()

    @api.model
    def create(self, values):
        product = self.env['product.product'].browse(values['product_id'])
        location_source_id = int(
            self.env['ir.config_parameter'].sudo().get_param('project_task_as_product.location_source_id'))
        location_source = self.env['stock.location'].browse(location_source_id)
        self.env['stock.quant']._update_available_quantity(product, location_source, -abs(values['quantity']))

        res = super(Task, self).create(values)

        return res

    @api.one
    @api.depends('product_id')
    def _compute_stock(self):
        self.stock = self.product_id.qty_available

    @api.onchange('product_id')
    def _onchange_product_id(self):
        self.product_uom_id = self.product_id.uom_id.id
        return {'domain': {'product_uom_id': [
            ('category_id', '=', self.product_id.uom_id.category_id.id)]}}

    # @api.multi
    # def write(self, vals):
    #     tmp_stock = self.stock
    #     product = self.env['product.product'].browse(self.product_id)
    #     _logger.debug(vals)
    #     location_source_id = int(
    #         self.env['ir.config_parameter'].sudo().get_param('project_task_as_product.location_source_id'))
    #     location_source = self.env['stock.location'].browse(location_source_id)
    #     _logger.debug('pppppppppppppppppppppppppp')
    #     _logger.debug(self.quantity)
    #     _logger.debug(vals['quantity'])
    #     _logger.debug('pppppppppppppppppppppppppp')
    #     # self.env['stock.quant']._update_available_quantity(product, location_source, -abs(vals['quantity']))
    #     res = super(Task, self).write(vals)
    #     return res

    @api.onchange('product_id')
    def _on_change_product_id(self):
        if self.product_id:
            self.price = self.product_id.lst_price
            self.name = self.product_id.name
            if self.product_id.qty_available < self.quantity:
                raise exceptions.Warning(
                    _("The quantity you entered is greater than the quantity available")
                )

    @api.onchange('quantity')
    def _on_change_quantity(self):
        if self.product_id:
            if self.product_id.qty_available < self.quantity:
                raise exceptions.Warning(
                    _("The quantity you entered is greater than the quantity available")
                )
