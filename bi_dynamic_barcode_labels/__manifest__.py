# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

{
    "name" : "Odoo Barcode Labels for All(Products,Templates,Sale,Purchase,Picking)",
    "version" : "12.0.0.4",
    "category" : "Sales",
    'summary': """This module helps to print dyanamic barcode labels from
                  Products | Templates | Sale | Purchase | Picking.
                  """,
    "description": """
                Print Dynamic Barcode Labels From
                Products | Templates | Sale | Purchase | Picking.
                BarCode Printing using Label Printer
                Print Dynamic Barcode Labels for Products,Templates,Sale,Purchase,Picking
                 Print Dynamic Barcode Labels for Templates,Sale,Purchase,Picking
                  Print Dynamic Barcode Labels for Sale,Purchase,Picking
                   Print Dynamic Barcode Labels for Purchase,Picking
                    Print Dynamic Barcode Labels for Picking
                    barcode label for products
                    Dynamic Barcode Labels for Products,Templates,Sale,Purchase,Picking
                    Dynamic Barcode Labels for Picking
                    Dynamic Barcode Labels for Sale,Purchase,Picking
                    print product barcode labels
                  product barcode labels
                  print picking barcode labels
               picking barcode labels
               print sale barcode labels
               sale barcode labels
            print sale barcode labels
            barcode product labels
            printing barcode labels
            Odoo Dynamic Barcode Labels for All(Products,Templates,Sale,Purchase,Picking) 
            Barcode Labels 
            print barcode labels print multiple barcode labels print quantity barcode labels print
            picking labels
            picking barcode labels
            
product labels
barcode for all
product barcode labels
This Odoo app module helps to print all sort dynamic barcode labels from Products, Templates, Sale, Purchase 
and, Pickings for printing barcode in Odoo.This Odoo apps helps to generate label according to each companies 
specification, each company has its own label size standard here with this help user can have option for 
customized label size template feature which is one time configurable. Based on configure barcode label height 
and width barcode label prints from Products Variants ,Product Templates,Sales Orders,Purchase Orders and Picking. 
Also it has most useful features to print multiple labels as per the quantity allocated on Orders or on Print section.


                    Barcode Labels for Products,Templates,Sale,Purchase,Picking


                """,
    "author": "BrowseInfo",
    "website" : "https://www.browseinfo.in",
    'price': 25,
    "currency": 'EUR',
    "depends" : ['base','sale_management','stock', 'purchase'],
    "data": [
        'security/ir.model.access.csv',
        'data/barcode_config_data.xml',
        'views/barcode_config_views.xml',
        'report/report_barcode_product_labels_temp.xml',
        'report/report_barcode_product_temp_labels.xml',
        'report/report_barcode_sale_labels.xml',
        'report/report_barcode_purchase_labels.xml',
        'report/report_barcode_stock_labels.xml',
        'report/report.xml',
        'wizard/barcode_product_labels_view.xml',
        'wizard/barcode_product_temp_labels_view.xml',
        'wizard/barcode_sales_labels_view.xml',
        'wizard/barcode_purchase_labels_view.xml',
        'wizard/barcode_stock_labels_view.xml',
         ],
    "auto_install": False,
    "installable": True,
    'live_test_url' :'https://youtu.be/fAohqTucCX4',
    "images":['static/description/Banner.png'],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
