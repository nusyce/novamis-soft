 v12.0.1 (31 July 2019)
 =========================
 Initial Release
 
 v12.0.2 (13 March 2020)
 ========================
 Features added,

1) Easy to scan products using a mobile scanner with Options (Barcode, QR, Internal Reference, All).
2) Barcode Format Supported UPC-E, EAN-8, EAN-13,Code 39,Code 93,Code 128.
3) QR Code Supported.
4) Continue Scanning Option.
5) Notification on product succeed & failed.
 
 v12.0.3 (06 August 2020)
 ========================
 Lot/Serial Number Barcode/QRCode Scanner Feature Added.
 
  v12.0.4 (7 Dec 2020)
 ========================
 ==> bug fixed: Internal Transfer scanning is supoorted in picking form.
 
 12.0.5 (25 Feb 2021)
==================
==> [Add] Scan Barcode and add into barcode field in product variant and template. 
==> [fix] choose of front and back camera not work properly.

12.0.6 (31 Dec 2021)
----------------------------
==> [FIX] Bug Fix.
