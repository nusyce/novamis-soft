About
============
Do you want to scan POS(Point Of Sale) products by Barcode or QRCode on your mobile? Do your time-wasting in POS(Point Of Sale) operations by manual product selection? So here are the solutions these modules useful do quick operations of POS mobile Barcode or QRCode scanner. You no need to select the product and do one by one. scan it and you do! So be very quick in all operations of odoo in mobile and cheers!

User Guide
============
Blog: https://www.softhealer.com/blog/odoo-2/post/pos-point-of-sale-mobile-barcode-qrcode-scanner-473

Note:
============
If you want to use the camera without SSL(https) in chrome(android) then please check "Use Camera Without SSL-https In Chrome Android" inside the module folder.

Installation
============
1) Copy module files to addon folder.
2) Restart odoo service (sudo service odoo-server restart).
3) Go to your odoo instance and open apps (make sure to activate debug mode).
4) click on update app list.
5) search module name and hit install button.

Any Problem with module?
=====================================
Please create your ticket here https://softhealer.com/support

Softhealer Technologies Doubt/Inquiry/Sales/Customization Team
=====================================
Skype: live:softhealertechnologies
What's app: +917984575681
E-Mail: support@softhealer.com
Website: https://softhealer.com
