12.0.3
-----------
Add configuration to check duplication of barcode in product and put constraint in multi barcodes and put multi barcode field in search view also.

12.0.4 (Date: 30th Dec 2020)
===============================
[FIX]if removed a barcode line from a product or delete a product of multi barcode then delete that barcode record also from multi barcode.

12.0.5 (Date : 9th June 2021)
---------------------------------
[ADD] add multi barcode tab in product.template also and it will worked as odoo standard flow.    