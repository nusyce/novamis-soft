# -*- coding: utf-8 -*-
# Copyright (C) Softhealer Technologies.

from odoo import models,fields,api,tools,_
from odoo.exceptions import ValidationError
from odoo.tools import pycompat

class ShProductTemplate(models.Model):
    _inherit='product.template'
    
    barcode_line_ids = fields.One2many(related='product_variant_ids.barcode_line_ids', readonly=False,ondelete='cascade')

    @api.model_create_multi
    def create(self, vals_list):
        templates = super(ShProductTemplate, self).create(vals_list)
        # This is needed to set given values to first variant after creation
        for template, vals in pycompat.izip(templates, vals_list):
            related_vals = {}
            if vals.get('barcode_line_ids'):
                related_vals['barcode_line_ids'] = vals['barcode_line_ids']
            if related_vals:
                template.write(related_vals)
        return templates

    @api.constrains('barcode','barcode_line_ids')
    def check_uniqe_name(self):
        for rec in self:
            if self.env.user.company_id and self.env.user.company_id.sh_multi_barcode_unique:
                multi_barcode_id = self.env['product.template.barcode'].search([('name', '=', rec.barcode)]) 
                if multi_barcode_id:
                    raise ValidationError(_(
                        'Barcode must be unique!'))

class ShProduct(models.Model):
    _inherit='product.product'
    
    barcode_line_ids = fields.One2many('product.template.barcode','product_id','Barcode Lines',ondelete='cascade')
    
    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        res = super(ShProduct, self)._name_search(name=name, args=args, operator=operator, limit=limit, name_get_uid=name_get_uid)
        mutli_barcode_search = self._search([('barcode_line_ids', '=', name)] + args, limit=limit, access_rights_uid=name_get_uid)
        multi_barcode_res=[]
        if mutli_barcode_search:
            multi_barcode_res = self.browse(mutli_barcode_search).name_get()
        return res + multi_barcode_res
    
    @api.constrains('barcode','barcode_line_ids')
    def check_uniqe_name(self):
        for rec in self:
            if self.env.user.company_id and self.env.user.company_id.sh_multi_barcode_unique:
                multi_barcode_id = self.env['product.template.barcode'].search([('name', '=', rec.barcode)]) 
                if multi_barcode_id:
                    raise ValidationError(_(
                        'Barcode must be unique!'))
    
class ShProductBarcode(models.Model):
    _name='product.template.barcode'
    _description="Product Barcode"
    
    product_id = fields.Many2one('product.product','Product',ondelete='cascade')
    name = fields.Char("Barcode",required=True,ondelete='cascade')

    @api.constrains('name')
    def check_uniqe_name(self):
        for rec in self:
            if self.env.user.company_id and self.env.user.company_id.sh_multi_barcode_unique:
                product_id = self.env['product.product'].sudo().search(['|',('barcode','=',rec.name),('barcode_line_ids.name','=',rec.name),('id','!=',rec.product_id.id)])
                if product_id:
                    raise ValidationError(_('Barcode must be unique!'))
                else:
                    barcode_id = self.env['product.template.barcode'].search([('name','=',rec.name),('id','!=',rec.id)])
                    if barcode_id:
                        raise ValidationError(_('Barcode must be unique!'))
