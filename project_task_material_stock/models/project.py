from odoo import fields, models, api
import logging

_logger = logging.getLogger(__name__)


class ProjectProject(models.Model):
    _inherit = 'project.project'

    location_source_id = fields.Many2one(
        comodel_name='stock.location',
        string='Source Location',
        index=True,
        help='Default location from which materials are consumed.',
    )
    location_dest_id = fields.Many2one(
        comodel_name='stock.location',
        string='Destination Location',
        index=True,
        help='Default location to which materials are consumed.',
    )

    total_price = fields.Float(string='Total Price', compute='_pj_compute_amount')

    @api.one
    @api.depends('task_ids.total_price')
    def _pj_compute_amount(self):
        self.total_price = sum(line.total_price for line in self.task_ids)
