# -*- coding: utf-8 -*-
# Part of Novaris. See LICENSE file for full copyright and licensing details.

{
    'name': 'Novaris12 Accounting',
    'version': '12.0.2.1.0',
    'category': 'Accounting',
    'summary': 'Accounting Reports, Asset Management and Account Budget For Novaris12 Community Edition',
    'sequence': '8',
    'author': 'Novaris Mates, Novaris SA',
    'website': 'http://odoomates.tech',
    'maintainer': 'Novaris Mates',
    'support': 'odoomates@gmail.com',
    'website': '',
    'depends': ['accounting_pdf_reports', 'om_account_asset', 'om_account_budget'],
    'demo': [],
    'data': [
        'wizard/change_lock_date.xml',
        'views/account.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'images': ['static/description/banner.gif'],
    'qweb': [],
}
