# -*- coding: utf-8 -*-
# Part of Novaris. See LICENSE file for full copyright and licensing details.

{
    'name': 'NovarisBot for livechat',
    'version': '1.0',
    'category': 'Discuss',
    'summary': 'Add livechat support for NovarisBot',
    'description': "",
    'website': 'https://www.nova-mis.com/page/discuss',
    'depends': ['mail_bot', 'im_livechat'],
    'installable': True,
    'application': False,
    'auto_install': True,
    'license': 'LGPL-3',
}
