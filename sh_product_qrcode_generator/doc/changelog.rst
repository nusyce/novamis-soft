12.0.1 (Date : 23 Jan 2020)
----------------------------
Initial Release

bug fixed: make qr code field as same as barcode like.


12.0.2 (Date: 19 Feb 2021)
--------------------------
[fix] float-left class added in oe_avatar to display image in left.

12.0.3 (Date: 2 March 2021)
--------------------------
[ADD] Generate QR Image when qr code manually changed.

12.0.4 (Date: 24 March 2021)
--------------------------
[ADD] QR Code fields and QR Code Image also visible in easy varint form.(when go to variant easy form direct from
product template form view using variant smart button.)


 12.0.5 (Date: 27 July 2021)
============================
 = [Fix] can not get barcode image when import products 

12.0.6 (Date: 18 Aug 2021)
============================
 = [Fix] product access rights error when sale own document user.

12.0.7 (Date: 29 Jan 2022)
============================
==> [update] Sql constraint add for QR code field 