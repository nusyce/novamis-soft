{
    'name': 'Nova-Mis Experts Theme',
    'description': 'Nova-Mis Experts Business Theme',
    'category': 'Theme/Corporate',
    'summary': 'Corporate, Services, Business, Finance, IT',
    'sequence': 210,
    'version': '1.0',
    'depends': ['theme_loftspace', 'website_animate'],
    'data': [
        'views/assets.xml',
        'views/images.xml',
        'views/snippets.xml',
    ],
    'images': [
        'static/description/odoo_experts_description.png',
        'static/description/odoo_experts_screenshot.jpeg',
    ],
    'price': 4,
    'currency': 'EUR',
    'live_test_url': 'https://theme-odoo-experts.nova-mis.com',
    'license': 'LGPL-3',
}
