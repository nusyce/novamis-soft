# -*- coding: utf-8 -*-
# Part of Nova-Mis. See LICENSE file for full copyright and licensing details.

{
    'name': 'Nova-Mis 12 Budget Management',
    'author': 'Nova-Mis Mates, Nova-Mis SA',
    'category': 'Accounting',
    'description': """Use budgets to compare actual with expected revenues and costs""",
    'summary': 'Nova-Mis 12 Budget Management',
    'website': 'http://odoomates.tech',
    'depends': ['account'],
    'data': [
        'security/ir.model.access.csv',
        'security/account_budget_security.xml',
        'views/account_analytic_account_views.xml',
        'views/account_budget_views.xml',
        'views/res_config_settings_views.xml',
    ],
    "images": ['static/description/banner.gif'],
    'demo': ['data/account_budget_demo.xml'],
}
