{
    'name': 'Theme Novaris Experts eCommerce Plugin',
    'description': 'Novaris Experts eCommerce Plugin',
    'category': 'Hidden',
    'sequence': 211,
    'version': '1.0',
    'depends': ['theme_loftspace_sale', 'theme_odoo_experts'],
    'auto_install': True,
    'license': 'LGPL-3',
}